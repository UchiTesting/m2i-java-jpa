package eu.fr.esic.formation.jpa.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import eu.fr.esic.formation.jpa.dao.ICommandeDAO;
import eu.fr.esic.formation.jpa.dao.IProduitDAO;
import eu.fr.esic.formation.jpa.dao.impl.CommandeDAOImpl;
import eu.fr.esic.formation.jpa.dao.impl.ProduitDAOImpl;
import eu.fr.esic.formation.jpa.entity.Commande;
import eu.fr.esic.formation.jpa.entity.Produit;

public class ProduitDAOTest {

	IProduitDAO produitDAO = new ProduitDAOImpl();
	ICommandeDAO commandeDAO = new CommandeDAOImpl();

	@Test
	public void testRecupAll() {
		List<Produit> listeProduit = this.produitDAO.findAll();
		listeProduit.forEach(System.out::println);
		Assert.assertEquals(11, listeProduit.size());
	}

	@Test
	public void testAddProduit() {
		String refProduit = "REF_PRD-CMD_10";
		Produit nouveauProduit = new Produit("HYUNDAI", "NISSAN", "NISSAN HYUNDAI Serie 2", 23480.0, refProduit);
		Commande commandeCinq = this.commandeDAO.getEntityById(5);

		nouveauProduit.setCommande(commandeCinq);

		Produit produitPersiste = this.produitDAO.createUpdateEntity(nouveauProduit);

		Assert.assertNotNull(produitPersiste);
	}

	// Q15
	@Test
	public void testPUAbove15K() {
		List<Produit> produits = this.produitDAO.findAbove15K();

		Assert.assertNotNull(produits);

		Assert.assertEquals(6, produits.size());
	}

	// Q16
	@Test
	public void testFromParisien() {
		List<Produit> produits = this.produitDAO.findFromParisien();

		Assert.assertNotNull(produits);
		Assert.assertEquals(3, produits.size());
	}

	// Q17
	@Test
	public void testMostExpensiveFromClient1() {
		Produit produit = this.produitDAO.findMostExpensiveFromClient1();

		Assert.assertNotNull(produit);
		// assertEquals pour double deprécié... ????? :/
		Assert.assertEquals(15000, produit.getPrixUnitaire().intValue());
	}
}
