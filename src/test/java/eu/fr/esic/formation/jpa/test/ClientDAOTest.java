package eu.fr.esic.formation.jpa.test;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.hibernate.mapping.Map;
import org.junit.Assert;
import org.junit.Test;

import eu.fr.esic.formation.jpa.dao.IClientDAO;
import eu.fr.esic.formation.jpa.dao.ICommandeDAO;
import eu.fr.esic.formation.jpa.dao.IProduitDAO;
import eu.fr.esic.formation.jpa.dao.impl.ClientDAOImpl;
import eu.fr.esic.formation.jpa.dao.impl.CommandeDAOImpl;
import eu.fr.esic.formation.jpa.dao.impl.ProduitDAOImpl;
import eu.fr.esic.formation.jpa.entity.Client;
import eu.fr.esic.formation.jpa.entity.Commande;
import eu.fr.esic.formation.jpa.entity.Produit;

public class ClientDAOTest {

	IClientDAO clientDAO = new ClientDAOImpl();
	ICommandeDAO commandeDAO = new CommandeDAOImpl();
	IProduitDAO produitDAO = new ProduitDAOImpl();

	@Test
	public void testRecupAll() {
		List<Client> listeClient = this.clientDAO.findAll();
		for (Client client : listeClient) {
			System.out.println(client);
		}
		Assert.assertTrue(listeClient.size() == 13);
	}

	@Test
	public void testRecupClientById() {
		// TODO : Recuperez ici le client d'identifiant 1
		Client client = this.clientDAO.getEntityById(1);
		Assert.assertNotNull(client);
		Assert.assertEquals(client.getNom(), "ZOME");
		System.out.println(client);
		// TODO : Recupere ses commandes
		List<Commande> listeCommandeClient = client.getCommandes();
		System.out.println("NB Commandes: " + listeCommandeClient.size());
		Assert.assertTrue(listeCommandeClient.size() == 2);
	}

	@Test
	public void testRecupClientParIdProduit() {
		Client clientDuProduit = null;
		// Méthode attendue sans JPQL
//		Client clientDuProduit = (produitDAO.getEntityById(4)).getCommande().getClient();

		// TODO : Recuperer le client ayant acheté le produit d'identifiant 4
		clientDuProduit = this.clientDAO.findClientByProductId(4);
		System.out.println(clientDuProduit);
		Assert.assertNotNull(clientDuProduit);
	}

	@Test
	public void testRecupClientParSexe() {
		List<Client> listeClient = this.clientDAO.findClientsParSexe(1);
		for (Client client : listeClient) {
			System.out.println(client);
		}
		// Neuf en base
		Assert.assertTrue(listeClient.size() == 9);
	}

	@Test
	public void testAddClient() {
		Commande nouvelleCommande = new Commande("CMD_07", 20);
		nouvelleCommande.setDate(new Date());

		Produit p1 = new Produit("MEGANE", "RENAULT", "RENAULT MEGANE COUPE", 16500.0, "REF_PRD_10");
		Produit p2 = new Produit("SERIE 3", "ALPHA ROMEO", "ALPHA ROMEO SERIE 3", 32000.0, "REF_PRD_10");

		nouvelleCommande.addProduit(p1);
		nouvelleCommande.addProduit(p2);

		Client nouveauClient = new Client("BARACK", "Obama", "barack", "123456", "32 Rue du Puisatier", "72010",
				"34289C", 1);
		nouveauClient.getCommandes().add(nouvelleCommande);

		nouvelleCommande.setClient(nouveauClient);

		// Erreur car ID_CLIENT Null. On ne le connait pas en avance.
		Client commandePersiste = this.clientDAO.createUpdateEntity(nouveauClient);

		Assert.assertNotNull(commandePersiste);

	}

	@Test
	public void testSupprimeClient() {
		Client barack = findAndReturnAnyClientByLogin("barack");

		Assert.assertNotNull(barack);

		this.clientDAO.deleteById(barack.getIdClient());

		barack = null;
		barack = findAndReturnAnyClientByLogin("barack");

		Assert.assertNull(barack);
	}

	private Client findAndReturnAnyClientByLogin(String clientLogin) {
		return this.clientDAO.findAll().stream().filter(c -> c.getLogin().equals(clientLogin)).findFirst().orElse(null);
	}

	@Test
	public void testRecupCommandesUser1() {
		Client client1 = this.clientDAO.getEntityById(1);

		Assert.assertNotNull(client1);

		List<Commande> commandes = client1.getCommandes();

		Assert.assertEquals(2, commandes.size());
	}

	@Test
	public void testRecupClientAyantCommandeProduit2AssertNotNull() {
		Produit produit = this.produitDAO.getEntityById(2);
		Client client = produit.getCommande().getClient();

		Assert.assertNotNull(client);

		System.out.println("Client ayant commandé produit 2 => " + client);
	}
}
