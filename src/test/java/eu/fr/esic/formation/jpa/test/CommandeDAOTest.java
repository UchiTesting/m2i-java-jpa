package eu.fr.esic.formation.jpa.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import eu.fr.esic.formation.jpa.dao.ICommandeDAO;
import eu.fr.esic.formation.jpa.dao.impl.CommandeDAOImpl;
import eu.fr.esic.formation.jpa.entity.Commande;
import eu.fr.esic.formation.jpa.entity.Produit;

public class CommandeDAOTest {

	ICommandeDAO commandeDAO = new CommandeDAOImpl();;

	@Test
	public void testRecupAll() {
		List<Commande> listeCommande = this.commandeDAO.findAll();
		for (Commande commande : listeCommande) {
			System.out.println(commande);
		}
		Assert.assertEquals(5, listeCommande.size());
	}

	@Test
	public void testRecupCommandeById() {
		// TODO : Recuperer la commande via le DAO correspondant
		Commande commande = this.commandeDAO.getEntityById(5);
		Assert.assertNotNull(commande);
		Assert.assertEquals(commande.getNumCommande(), "CMD_05");

		// TODO : Recuperer les produits correspondants
		List<Produit> listeProduit = commande.getProduits();
		Assert.assertEquals(4, listeProduit.size());
	}

	@Test
	public void testSupprimerCommande() {
		Commande commande = this.commandeDAO.getEntityById(21);
		Assert.assertNotNull(commande);
		System.out.println(commande);
		this.commandeDAO.deleteById(21);
	}

	@Test
	public void testCommande5Possede9Produits() {
		Commande commande5 = this.commandeDAO.getEntityById(5);

		Assert.assertNotNull(commande5);

		List<Produit> produits = commande5.getProduits();
		Assert.assertNotNull(produits);
		// Sujet dit 9. En réalité 4 produits en base pour la commande 5
		Assert.assertEquals(4, produits.size());
	}
}
