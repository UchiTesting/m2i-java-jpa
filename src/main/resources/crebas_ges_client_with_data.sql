-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ges_client
-- -----------------------------------------------------
-- Base de gestion des clients
DROP SCHEMA IF EXISTS `ges_client` ;

-- -----------------------------------------------------
-- Schema ges_client
--
-- Base de gestion des clients
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ges_client` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ges_client` ;

--
-- Base de données :  `ges_client`
--

-- --------------------------------------------------------

--
-- Structure de la table `CLIENT`
--

CREATE TABLE `CLIENT` (
`ID_CLIENT` int(11) NOT NULL,
  `NUM_CLIENT` varchar(45) NOT NULL COMMENT 'Numéro metier identifiant le client',
  `NOM` varchar(45) NOT NULL,
  `PRENOM` varchar(45) DEFAULT NULL,
  `LOGIN` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) NOT NULL,
  `SEXE` int(11) NOT NULL,
  `ADRESSE` varchar(45) DEFAULT NULL,
  `CODE_POSTAL` varchar(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLIENT`
--

INSERT INTO `CLIENT` (`ID_CLIENT`, `NUM_CLIENT`, `NOM`, `PRENOM`, `LOGIN`, `PASSWORD`, `SEXE`, `ADRESSE`, `CODE_POSTAL`) VALUES
(1, '1254C', 'ZOME', 'Christophe', 'christophe', '123456', 1, '22 Rue Marechal Le FOCH', '35700'),
(2, '56983J', 'LECLERC', 'Edouard', 'edouard', '123456', 1, '45 Rue François PIGNON', '75012'),
(3, '58967O', 'MAZURAIS', 'Ludivine', 'ludivine', '123456', 2, '15 Rue Marechal Le Foch', '35640'),
(6, '56789U', 'SARKOZY', 'Nicolas', 'nicolas', '123456', 1, '34 Avenue Martin Luther King', '95360'),
(7, '56776K', 'TARTAMPION', 'Yves Maieul', 'yves', '123456', 1, '56 Boulevard Jean Luc', '44230'),
(8, '26789N', 'LOINSARD', 'Olivia', 'olivia', '123456', 2, '89 Rue Jean Jaurès', '56100'),
(9, '10789Q', 'LETERTRE', 'Yannick', 'yannick', '123456', 1, '56 Place de Corps-Nuds', '35600'),
(10, '90787G', 'QUEGUINER', 'Charles', 'charles', '123456', 1, '32 Rue COMPOSTEL', '75020'),
(11, '12089V', 'PLACIDE', 'Fresnais', 'fresnais', '123456', 1, '23 Rue LEGRAND MARCHAL', '72001'),
(12, '12299A', 'SHEMATSI', 'Johnny', 'johnny', '123456', 1, '45 RUE DU LOYAL', '72010'),
(13, '12089V', 'NEVO', 'Stéphane', 'stephane', '123456', 1, '33 RUE DU CLERC', '44000'),
(14, '12285D', 'CHEVAL', 'Martine', 'martine', '123456', 2, '67 Rue du vin', '33063'),
(15, '300890G', 'ELBAZ', 'Michelle', 'michelle', '123456', 2, '167 Rue Vercingétorix', '75016');

-- --------------------------------------------------------

--
-- Structure de la table `COMMANDE`
--

CREATE TABLE `COMMANDE` (
`ID_COMMANDE` int(11) NOT NULL,
  `NUM_COMMANDE` varchar(45) NOT NULL,
  `DATE` datetime NOT NULL,
  `QUANTITE` int(11) DEFAULT NULL,
  `ID_CLIENT` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `COMMANDE`
--

INSERT INTO `COMMANDE` (`ID_COMMANDE`, `NUM_COMMANDE`, `DATE`, `QUANTITE`, `ID_CLIENT`) VALUES
(1, 'CMD_01', '2015-11-10 15:00:00', 10, 1),
(2, 'CMD_02', '2015-12-09 10:00:00', 30, 1),
(3, 'CMD_03', '2016-01-09 09:00:00', 40, 2),
(4, 'CMD_04', '2016-02-15 14:30:00', 30, 2),
(5, 'CMD_05', '2016-06-17 17:30:00', 5, 10);

-- --------------------------------------------------------

--
-- Structure de la table `PRODUIT`
--

CREATE TABLE `PRODUIT` (
`ID_PRODUIT` int(11) NOT NULL,
  `REF_PRODUIT` varchar(45) DEFAULT NULL,
  `MARQUE` varchar(45) DEFAULT NULL,
  `GAMME` varchar(45) DEFAULT NULL,
  `LIBELLE` varchar(45) DEFAULT NULL,
  `PRIX_UNITAIRE` double DEFAULT NULL,
  `ID_COMMANDE` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PRODUIT`
--

INSERT INTO `PRODUIT` (`ID_PRODUIT`, `REF_PRODUIT`, `MARQUE`, `GAMME`, `LIBELLE`, `PRIX_UNITAIRE`, `ID_COMMANDE`) VALUES
(2, 'REF_PRD01', 'RENAULT', 'LAGUNA', 'RENAULT LAGUNA COUPE', 7000, 1),
(3, 'REF_PRD02', 'RENAULT', 'MEGANE', 'RENAULT MEGANE I', 8900, 1),
(4, 'REF_PRD03', 'BMW', 'Serie 1', 'BMW Serie 1 Berline', 15000, 2),
(5, 'REF_PRD04', 'FORD', 'FIESTA', 'FORD FIESTA Break', 12000, 2),
(6, 'REF_PRD05', 'FORD', 'CMAX', 'FORD CMAX MONOSPACE', 18000, 3),
(7, 'REF_PRD06', 'NISSAN', 'QASHQAI', 'NISSAN QASHQAI ACENTA', 12500, 4),
(8, 'REF_PRD07', 'NISSAN', 'EVALIA', 'NISSAN EVALIA BERLINE', 23000, 4),
(9, 'REF_PRD07', 'NISSAN', 'JUKE', 'NISSAN JUKE CROSSOVER', 30000, 5),
(10, 'REF_PRD08', 'PEUGEOT', '108', 'PEUGEOT 108 CITADINE', 22000, 5),
(11, 'REF_PRD09', 'PEUGEOT', '2008', 'PEUGEOT 2008 CROSSOVER', 32000, 5);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
 ADD PRIMARY KEY (`ID_CLIENT`);

--
-- Index pour la table `COMMANDE`
--
ALTER TABLE `COMMANDE`
 ADD PRIMARY KEY (`ID_COMMANDE`), ADD KEY `fk_COMMANDE_Client_idx` (`ID_CLIENT`);

--
-- Index pour la table `PRODUIT`
--
ALTER TABLE `PRODUIT`
 ADD PRIMARY KEY (`ID_PRODUIT`), ADD KEY `fk_PRODUIT_COMMANDE1_idx` (`ID_COMMANDE`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
MODIFY `ID_CLIENT` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `COMMANDE`
--
ALTER TABLE `COMMANDE`
MODIFY `ID_COMMANDE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `PRODUIT`
--
ALTER TABLE `PRODUIT`
MODIFY `ID_PRODUIT` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `COMMANDE`
--
ALTER TABLE `COMMANDE`
ADD CONSTRAINT `fk_COMMANDE_Client` FOREIGN KEY (`ID_CLIENT`) REFERENCES `Client` (`ID_CLIENT`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PRODUIT`
--
ALTER TABLE `PRODUIT`
ADD CONSTRAINT `fk_PRODUIT_COMMANDE1` FOREIGN KEY (`ID_COMMANDE`) REFERENCES `COMMANDE` (`ID_COMMANDE`) ON DELETE NO ACTION ON UPDATE NO ACTION;
