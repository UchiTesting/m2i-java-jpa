package eu.fr.esic.formation.jpa.dao.impl;

import java.util.List;

import javax.persistence.Query;

import eu.fr.esic.formation.jpa.dao.IProduitDAO;
import eu.fr.esic.formation.jpa.entity.Produit;

public class ProduitDAOImpl extends AbstractEntityImpl<Produit> implements IProduitDAO {
	public ProduitDAOImpl() {
		super(Produit.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Produit> findAbove15K() {
		String namedQuery = CustomNamedQueries.produitFindPUAbove15K;
		Query query = this.getEntityManager().createNamedQuery(namedQuery);
		List<Produit> produits = (List<Produit>) query.getResultList();

		return produits;
	}

	@SuppressWarnings("unchecked")
	public List<Produit> findFromParisien() {
		String namedQuery = CustomNamedQueries.produitFindCommandeParisienAbove10;
		Query query = this.getEntityManager().createNamedQuery(namedQuery);

		List<Produit> produits = (List<Produit>) query.getResultList();
		return produits;
	}

	public Produit findMostExpensiveFromClient1() {
		String namedQuery = CustomNamedQueries.produitFindMostExpensiveClient1;
		Query query = this.getEntityManager().createNamedQuery(namedQuery);
		Produit produit = (Produit) query.setMaxResults(1).getSingleResult();

		return produit;
	}
}
