package eu.fr.esic.formation.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.fr.esic.formation.jpa.dao.impl.CustomJpqlQueries;
import eu.fr.esic.formation.jpa.dao.impl.CustomNamedQueries;


/**
 * The persistent class for the COMMANDE database table.
 * 
 */
@Entity
@NamedQuery(name=CustomNamedQueries.commandeFindAll, query=CustomJpqlQueries.commandeFindAllQuery)
@Embeddable
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_COMMANDE")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idCommande;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CLIENT")
	private Client client;

	@Column(name="NUM_COMMANDE")
	private String numCommande;
	
	@Column(name="QUANTITE")
	private Integer quantite;

	//bi-directional many-to-one association to Produit
	@OneToMany(mappedBy = "commande", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	private List<Produit> produits;

	public Commande() {
	}
	
	public Commande(String numC,int qte) {
		this.numCommande = numC;
		this.quantite = qte;
		this.date = new Date();
		// Was missing so NullPointerException upon test.
		this.produits = new ArrayList<>();
	}

	public Integer getIdCommande() {
		return this.idCommande;
	}

	public void setIdCommande(Integer idCommande) {
		this.idCommande = idCommande;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNumCommande() {
		return this.numCommande;
	}

	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}

	public Integer getQuantite() {
		return this.quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	public List<Produit> getProduits() {
		return this.produits;
	}

	public void setProduits(List<Produit> produits) {
		this.produits = produits;
	}

	public Produit addProduit(Produit produit) {
		getProduits().add(produit);
		produit.setCommande(this);

		return produit;
	}

	public Produit removeProduit(Produit produit) {
		getProduits().remove(produit);
		produit.setCommande(null);

		return produit;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public String toString(){
		return "NUM_COMMANDE : "+this.numCommande + " QUANTITE : "+this.quantite;
	}

}