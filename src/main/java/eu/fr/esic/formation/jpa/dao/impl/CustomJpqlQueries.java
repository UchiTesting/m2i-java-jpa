package eu.fr.esic.formation.jpa.dao.impl;

public class CustomJpqlQueries {
	/**
	 * CLIENT
	 */
	public static final String clientFindAllQuery = "SELECT c FROM Client c";

	public static final String clientFindClientParSexeQuery = "SELECT c FROM Client c WHERE c.sexe = :paramSexe";

	public static final String clientFindClientWithProduitIdQuery = "SELECT p FROM Produit p"
			+ " WHERE p.idProduit = :paramProduit";

	/**
	 * COMMANDE
	 */
	public static final String commandeFindAllQuery = "SELECT c FROM Commande c";

	/**
	 * PRODUIT
	 */
	public static final String produitFindAllQuery = "SELECT p FROM Produit p";

	public static final String produitFindPUAbove15KQuery = "SELECT p FROM Produit p WHERE p.prixUnitaire > 15000";

	public static final String produitFindCommandeParisienAbove10Query = "SELECT p " + "FROM Produit p "
			+ "JOIN p.commande co " + "JOIN co.client cl " + "WHERE cl.codePostal IN('75012', '75016', '75020') "
			+ "AND p.commande.quantite > 10";

	public static final String produitFindMostExpensiveClient1Query = "SELECT p " + "FROM Produit p "
			+ "JOIN p.commande co " + "JOIN co.client cl " + "WHERE cl.idClient = 1 " + "ORDER BY p.prixUnitaire DESC";
}
