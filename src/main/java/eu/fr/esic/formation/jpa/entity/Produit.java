package eu.fr.esic.formation.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import eu.fr.esic.formation.jpa.dao.impl.CustomJpqlQueries;
import eu.fr.esic.formation.jpa.dao.impl.CustomNamedQueries;

/**
 * The persistent class for the PRODUIT database table.
 * 
 */
@Entity
@Embeddable
@NamedQueries({ @NamedQuery(name = CustomNamedQueries.produitFindAll, query = CustomJpqlQueries.produitFindAllQuery),
		@NamedQuery(name = CustomNamedQueries.produitFindPUAbove15K, query = CustomJpqlQueries.produitFindPUAbove15KQuery),
		@NamedQuery(name = CustomNamedQueries.produitFindCommandeParisienAbove10, query = CustomJpqlQueries.produitFindCommandeParisienAbove10Query),
		@NamedQuery(name = CustomNamedQueries.produitFindMostExpensiveClient1, query = CustomJpqlQueries.produitFindMostExpensiveClient1Query) })

public class Produit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PRODUIT")
	private Integer idProduit;

	@Column(name = "GAMME")
	private String gamme;

	@Column(name = "LIBELLE")
	private String libelle;

	@Column(name = "MARQUE")
	private String marque;

	@Column(name = "PRIX_UNITAIRE")
	private Double prixUnitaire;

	@Column(name = "REF_PRODUIT")
	private String refProduit;

	@Embedded
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_COMMANDE")
	private Commande commande;

	public Produit() {
	}

	public Produit(String gamme, String marque, String lib, Double prixU, String refP) {
		this.gamme = gamme;
		this.marque = marque;
		this.libelle = lib;
		this.prixUnitaire = prixU;
		this.refProduit = refP;
	}

	public Integer getIdProduit() {
		return this.idProduit;
	}

	public void setIdProduit(Integer idProduit) {
		this.idProduit = idProduit;
	}

	public String getGamme() {
		return this.gamme;
	}

	public void setGamme(String gamme) {
		this.gamme = gamme;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getMarque() {
		return this.marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public Double getPrixUnitaire() {
		return this.prixUnitaire;
	}

	public void setPrixUnitaire(Double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public String getRefProduit() {
		return this.refProduit;
	}

	public void setRefProduit(String refProduit) {
		this.refProduit = refProduit;
	}

	public Commande getCommande() {
		return this.commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	public String toString() {
		return "Produit [REF_PRODUIT : " + this.refProduit + "]" + " a pour Libelle :" + this.libelle;
	}

}