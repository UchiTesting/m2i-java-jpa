package eu.fr.esic.formation.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import eu.fr.esic.formation.jpa.dao.impl.CustomJpqlQueries;
import eu.fr.esic.formation.jpa.dao.impl.CustomNamedQueries;

/**
 * The persistent class for the CLIENT database table.
 */
@Entity
@NamedQueries({ @NamedQuery(name = CustomNamedQueries.clientFindAll, query = CustomJpqlQueries.clientFindAllQuery),
		@NamedQuery(name = CustomNamedQueries.clientFindClientParSexe, query = CustomJpqlQueries.clientFindClientParSexeQuery),
		@NamedQuery(name = CustomNamedQueries.clientFindClientWithProduitId, query = CustomJpqlQueries.clientFindClientWithProduitIdQuery) })
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CLIENT")
	private Integer idClient;

	private String adresse;

	@Column(name = "CODE_POSTAL")
	private String codePostal;

	private String nom;

	private String login;

	private String password;

	@Column(name = "NUM_CLIENT")
	private String numClient;

	private String prenom;

	private int sexe;

	// bi-directional many-to-one association to Produit
	@OneToMany(mappedBy = "client", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<Commande> commandes;

	public Client() {
	}

	public Client(String nom, String prenom, String login, String password, String adresse, String codeP, String numCli,
			int sex) {
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.adresse = adresse;
		this.codePostal = codeP;
		this.numClient = numCli;
		this.sexe = sex;
		this.commandes = new ArrayList<>();
	}

	public Integer getIdClient() {
		return this.idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return this.codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNumClient() {
		return this.numClient;
	}

	public void setNumClient(String numClient) {
		this.numClient = numClient;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getSexe() {
		return this.sexe;
	}

	public void setSexe(int sexe) {
		this.sexe = sexe;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public String toString() {
		String genre = this.sexe == 1 ? "M. " : "Mme. ";
		return genre + this.nom + " " + this.prenom + " [NUM_CLIENT :" + this.numClient + "] Habite " + this.adresse;
	}

}
