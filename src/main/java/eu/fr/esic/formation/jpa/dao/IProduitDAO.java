package eu.fr.esic.formation.jpa.dao;

import java.util.List;

import eu.fr.esic.formation.jpa.entity.Produit;

public interface IProduitDAO extends IAbstractEntityDAO<Produit> {
	List<Produit> findAbove15K();

	List<Produit> findFromParisien();

	Produit findMostExpensiveFromClient1();
}
