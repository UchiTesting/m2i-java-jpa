package eu.fr.esic.formation.jpa.dao.impl;

public class CustomNamedQueries {
	/**
	 * CLIENT
	 */
	// Ne pas changer la valeur. Elle dépends de l'implémentation dans
	// AbstractEntityImpl
	public static final String clientFindAll = "Client.findAll";
	public static final String clientFindClientParSexe = "Client.findClientParSexe";
	public static final String clientFindClientWithProduitId = "Client.findClientWithProduitId";

	/**
	 * COMMANDE
	 */
	public static final String commandeFindAll = "Commande.findAll";

	/**
	 * PRODUIT
	 */
	public static final String produitFindAll = "Produit.findAll";
	public static final String produitFindPUAbove15K = "Produit.findPUAbove15K";
	public static final String produitFindCommandeParisienAbove10 = "Produit.findCommandeParisienAbove10";
	public static final String produitFindMostExpensiveClient1 = "Produit.findMostExpensiveClient1";
}
