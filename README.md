M2i Java JPA Practicum
======================

Repository of my work on the introductory JPA practicum on my Java training at M2i Formation.

> Tests are reflecting the state of my local setup.  
> Should they fail, make sure to compare with actual data.
